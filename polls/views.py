from django.http.response import HttpResponse
from django.shortcuts import render

def index(request):
    return HttpResponse("Hello, world. You're at the polls index.")

def poll_by_id(request):
    return HttpResponse("poll by id")